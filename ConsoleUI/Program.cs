﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MatrixLib;
using MatrixLib.Matrix;
using MatrixLib.Vector;

namespace ConsoleUI
{
    class Program
    {
        private static Matrix _c = new Matrix(1, 3).Fill(12, 55);

        static void Main(string[] args)
        {
            try
            {

                Console.WriteLine(_c);
                Console.WriteLine(_c[0,0]);
                int Main_result = _c[0, 0] * 2;
                ;
                Matrix Main_mat = _c * 3;
                ;
                Console.WriteLine(Main_result);
                Console.WriteLine(Main_mat);
                int Main_det = Matrix.Determinant(_c);
                ;
                Console.WriteLine(Main_det);



                //MatrixTest();
                //VectorTest();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }


        static void VectorTest()
        {
            Vector v1 = new Vector(2);
            //var nums = new List<int>() { 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            v1.Fill(2,3,4,5);
            Console.WriteLine(v1.ToString());
            Console.WriteLine("---------------------------");

            Vector v2 = new Vector(3);
            var nums1 = new List<int>() { 5,6,7,8,9,10};
            v2.Fill(nums1);
            Console.WriteLine(v2.ToString());
            Console.WriteLine("---------------------------");

            Vector v3 = v1 * 2;
            Console.WriteLine(v3.ToString());
            Console.WriteLine("---------------------------");
            Console.WriteLine(Vector.Length(v1));
            Console.WriteLine((2+3*Math.Pow(2,5)).ToString());
        }



        static void MatrixTest()
        {
            Matrix m1 = new Matrix(3, 3);
            var nums = new List<int>() { 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            m1.Fill(22,23,11,44,55,77);
            Console.WriteLine(m1.ToString());
            Console.WriteLine("---------------------------");

            Matrix m2 = new Matrix(2, 3);
            //nums = new List<int>() { 25, 24, 23, 22, 21, 20, 19, 18, 17 };
            m2.Fill(nums);
            Console.WriteLine(m2.ToString());
            Console.WriteLine("---------------------------");

            //var m3 = m1 * m2;
            //Console.WriteLine(m3.ToString());

            Console.WriteLine(Matrix.Determinant(m1));
            Console.WriteLine(m1 [ 2 , 1 ] * 1000);
        }
    }
}
