﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixLib
{
    public class YuchNumber
    {
        public int Num { get; set; }

        public static YuchNumber operator +(YuchNumber yuchNumber1, YuchNumber yuchNumber2)
        {
            var yuchResult = new YuchNumber();
            yuchResult.Num = yuchNumber1.Num + yuchNumber2.Num;
            return yuchResult;
        }
    }
}
