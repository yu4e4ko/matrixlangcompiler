﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixLib.Matrix
{
    public static class MatrixHelper
    {
        public static Matrix AddMatrix(this Matrix m1, Matrix m2)
        {
            if (!m1.IsMatrixSizeEqual(m2))
            {
                throw new ArgumentException("Sizes of matrixes is not equals");
            }
            var resultMatrix = new Matrix(m1.sizeX, m1.sizeY);
            for (int i = 0; i < m1.sizeX; i++)
            {
                for (int j = 0; j < m1.sizeY; j++)
                {
                    resultMatrix[i, j] = m1[i,j] + m2[i,j];
                }
            }
            return resultMatrix;
        }


        public static Matrix MinusMatrix(this Matrix m1, Matrix m2)
        {
            if (!m1.IsMatrixSizeEqual(m2))
            {
                throw new ArgumentException("Sizes of matrixes is not equals");
            }
            var resultMatrix = new Matrix(m1.sizeX, m1.sizeY);
            for (int i = 0; i < m1.sizeX; i++)
            {
                for (int j = 0; j < m1.sizeY; j++)
                {
                    resultMatrix[i, j] = m1[i, j] - m2[i, j];
                }
            }
            return resultMatrix;
        }


        public static Matrix MultMatrixByInt(this Matrix m1, int number)
        {
            var resultMatrix = new Matrix(m1.sizeX, m1.sizeY);
            for (int i = 0; i < m1.sizeX; i++)
            {
                for (int j = 0; j < m1.sizeY; j++)
                {
                    resultMatrix[i, j] = m1[i, j] * number;
                }
            }
            return resultMatrix;
        }


        public static Matrix MultMatrix(this Matrix m1, Matrix m2)
        {
            if (!m1.IsMatrixForMult(m2))
            {
                throw new ArgumentException("matrix is not multiplied");
            }

            var resultMatrix = new Matrix(m1.sizeX, m2.sizeY);
            for (int i = 0; i < m1.sizeX; i++)
            {
                for (int j = 0; j < m2.sizeY; j++)
                {
                    for (int k = 0; k < m2.sizeX; k++)
                    {
                        resultMatrix[i, j] += m1[i, k] * m2[k, j];
                    }
                }
            }
            return resultMatrix;
        }



        /// thank you google!
        public static int Determinant(this Matrix mat, int c)
        {
            int result = 0;
 
            // Если у нас матрица из одного столбца и одной строки - возвращаем элемент нулевого столбца нулевой строки, который у этой матрицы единственный (отсчёт ведётся с нуля)
            if (c == 1)
            {
                result = mat[0, 0];
                return result;
            }
 
            // Если у нас матрица 2х2, то возвращаем разность произведений элементов главной и побочной диагоналей. Тоже ничего сложного
            if (c == 2)
            {
                result = mat[0, 0] * mat[1, 1] - mat[0, 1] * mat[1, 0];
                return result;
            }
 
            // Вот тут начинается жара))
            // Пробегаемся переменной i от 0 до (количество элементов в строке минус один) - в общем, по всем индексам элементов строки
            if (c > 2)
            {
                for (int i = 0; i < c; i++)
                {
                    // создаём временный массив temp из вещественных чисел размером [кол. элементов с столбце минус один Х кол. эл. в строке минус один]
                    //int[,] temp = new int[c - 1, c - 1];
                    Matrix temp = new Matrix(c - 1, c - 1);
                    // Раскладываем матрицу по столбцу: Мы должны сформировать матрицу для подсчёта минора для элемента матрицы a[0][i]
                    // Минор для элемента a[j][i] - это определитель матрицы, получающейся, если выбросить строку и столбец,
                    //в которых располагается элемент a[j][i]
                    // Получается, мы выбрасываем из матрицы верхнюю строчку и столбец i
                   
                    for(int j = 0; j < c-1; j++)
                    {
                        for (int k = 0; k < i; k++)              
                            temp[j,k] = mat[j+1,k];
                        for (int k = i + 1; k < c; k++)
                            temp[j,k-1] = mat [j+1,k];    
                    }                                    
                        // Из каждой последующей строчки нам надо скопировать в новый массив temp элементы,
//которые идут до i-го элемента и элементы, которые идут после i-го элемента. Но не сам i-й элемент.
                        // получили массив temp
                        /* теперь по определению, нам надо сложить все произведения
                        1) элементов a[0][i], i пробегает по всем индексам массива, на
                        2) соответствующий элементу a[0][i] минор и на
                        3) знак перестановки
                        */
 
                        // чтобы получить минор, нам надо найти определитель матрицы temp
                        // чтобы получить знак перестановки, нам надо возвести число (-1) в степень i(индекс, а не комплексное число). Почему это так, я не помню, но это как-то доказывается в теореме о разложении по столбцу/строке
                        // Ну и вот, прибавляем к уже существующему результату элемент матрицы a[0][i], умноженный на знак перестановки и умноженный на определитель матрицы temp.
                    result += (int) (mat[0,i] * Math.Pow(-1, i) * Determinant(temp, c - 1));
                   
                }
           
            }
            return result;
        }

        public static bool IsMatrixForMult(this Matrix m1, Matrix m2)
        {
            return m1.sizeX == m2.sizeY && m1.sizeY == m2.sizeX;
        }

        public static bool IsMatrixSizeEqual(this Matrix m1, Matrix m2)
        {
            return m1.sizeX == m2.sizeX && m1.sizeY == m2.sizeY;
        }
    }
}
