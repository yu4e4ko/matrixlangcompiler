﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixLib.Matrix
{
    public class Matrix
    {
        internal int[,] matrix;
        internal readonly int sizeX;
        internal readonly int sizeY;

        public int this[int i, int j]
        {
            get
            {
                Expect.MatrixIndexIsNotOutOfRange(this, i, j);
                return matrix[i, j];
            }
            set { matrix[i, j] = value; }
        }

        public Matrix(int sizeX, int sizeY)
        {
            this.sizeX = sizeX;
            this.sizeY = sizeY;
            matrix = new int[sizeX,sizeY];
        }

        public Matrix Fill(List<int> numbers)
        {
            Expect.EnumerableNotNull(numbers as IEnumerable<int>, "fillMatrixList");
            var listIndex = 0;
            var filled = false;
            for (int i = 0; i < sizeX; i++)
            {
                if (filled)
                {
                    break;
                }
                for (int j = 0; j < sizeY; j++)
                {
                    if (filled)
                    {
                        break;
                    }
                    matrix[i, j] = numbers[listIndex];
                    if (numbers.Count > listIndex + 1)
                    {
                        listIndex++;
                    }
                    else
                    {
                        filled = true;
                    }
                }
            }
            return this;
        }


        public Matrix Fill(params int[] numbers)
        {
            Expect.EnumerableNotNull(numbers as IEnumerable<int>, "fillMatrixList");
            var listIndex = 0;
            var filled = false;
            for (int i = 0; i < sizeX; i++)
            {
                if (filled)
                {
                    break;
                }
                for (int j = 0; j < sizeY; j++)
                {
                    if (filled)
                    {
                        break;
                    }
                    matrix[i, j] = numbers[listIndex];
                    if (numbers.Length > listIndex + 1)
                    {
                        listIndex++;
                    }
                    else
                    {
                        filled = true;
                    }
                }
            }
            return this;
        }


        public override string ToString()
        {
            var result = string.Empty;
            for (int i = 0; i < sizeX; i++)
            {
                for (int j = 0; j < sizeY; j++)
                {
                    result += matrix[i, j].ToString();
                    if (j < sizeY - 1)
                    {
                        result += ", ";
                    }
                }
                result += Environment.NewLine;
            }
            return result;
        }



        ///////////////////////////////////
        public static Matrix operator +(Matrix m1, Matrix m2)
        {
            return m1.AddMatrix(m2);
        }


        public static Matrix operator -(Matrix m1, Matrix m2)
        {
            return m1.MinusMatrix(m2);
        }


        public static Matrix operator *(Matrix m1, int num)
        {
            return m1.MultMatrixByInt(num);
        }

        public static Matrix operator *(int num, Matrix m1)
        {
            return m1.MultMatrixByInt(num);
        }


        public static Matrix operator *(Matrix m1, Matrix m2)
        {
            return m1.MultMatrix(m2);
        }

        public static int Determinant(Matrix m1)
        {
            if (m1.sizeX != m1.sizeY)
            {
                throw new ArgumentException("this matrix don't have determinant");
            }
            return m1.Determinant(m1.sizeX);
        }

    }
}
