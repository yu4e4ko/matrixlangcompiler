﻿using System;
using System.Collections.Generic;


namespace MatrixLib
{
    class Expect
    {
        public static void EnumerableNotNull<T>(IEnumerable<T> enumerable, string argName)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(argName + " is null");
            }
        }


        public static void MatrixIndexIsNotOutOfRange(Matrix.Matrix m, int i, int j)
        {
            if (m.sizeX <= i || m.sizeY <= j)
            {
                throw new ArgumentException("indexes out of range");
            }
        }


        public static void VectorIndexIsNotOutOfRange(Vector.Vector v, int i)
        {
            if (v.vectorSize <= i)
            {
                throw new ArgumentException("indexes out of range");
            }
        }
    }
}
