﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixLib.Vector
{
    internal static class VectorHelper
    {
        public static Vector AddVector(this Vector v1, Vector v2)
        {
            if (!v1.IsSizeEqual(v2))
            {
                throw new ArgumentException("Sizes of vectors is not equals");
            }
            var resultVector = new Vector(v1.vectorSize);
            for (int i = 0; i < resultVector.vectorSize; i++)
            {
                resultVector[i] = v1[i] + v2[i];
            }
            return resultVector;
        }


        public static Vector MinusVector(this Vector v1, Vector v2)
        {
            if (!v1.IsSizeEqual(v2))
            {
                throw new ArgumentException("Sizes of vectors is not equals");
            }
            var resultVector = new Vector(v1.vectorSize);
            for (int i = 0; i < resultVector.vectorSize; i++)
            {
                resultVector[i] = v1[i] - v2[i];
            }
            return resultVector;
        }


        public static Vector MultVectorByInt(this Vector v1, int num)
        {
            var resultVector = new Vector(v1.vectorSize);
            for (int i = 0; i < resultVector.vectorSize; i++)
            {
                resultVector[i] = v1[i] * num;
            }
            return resultVector;
        }


        public static int MultVector(this Vector v1, Vector v2)
        {
            if (!v1.IsSizeEqual(v2))
            {
                throw new ArgumentException("Sizes of vectors is not equals");
            }
            var result = 0;
            for (int i = 0; i < v1.vectorSize; i++)
            {
                result += v1[i] * v2[i];
            }
            return result;
        }


        public static double Determinant(this Vector v1)
        {
            double result = 0;
            for (int i = 0; i < v1.vectorSize; i++)
            {
                result += Math.Pow(v1[i], 2);
            }
            result = Math.Pow(result, 0.5);
            return result;
        }

        private static bool IsSizeEqual(this Vector v1, Vector v2)
        {
            return v1.vectorSize == v2.vectorSize;
        }
    }
}
