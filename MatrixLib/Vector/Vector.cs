﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixLib.Vector
{
    public class Vector
    {
        internal int[] vector;
        internal int vectorSize;

        public Vector(int size)
        {
            vectorSize = size;
            vector = new int[vectorSize];
        }

        public int this[int i]
        {
            get
            {
                Expect.VectorIndexIsNotOutOfRange(this, i);
                return vector[i];
            }
            set { vector[i] = value; }
        }


        public Vector Fill(List<int> numbers)
        {
            Expect.EnumerableNotNull(numbers as IEnumerable<int>, "fillVectorList");
            var listIndex = 0;
            for (int i = 0; i < vectorSize; i++)
            {
                vector[i] = numbers[listIndex];
                if (numbers.Count > listIndex + 1)
                {
                    listIndex++;
                }
                else
                {
                    break;
                }
            }
            return this;
        }


        public Vector Fill(params int[] numbers)
        {
            Expect.EnumerableNotNull(numbers as IEnumerable<int>, "fillVectorList");
            var listIndex = 0;
            for (int i = 0; i < vectorSize; i++)
            {
                vector[i] = numbers[listIndex];
                if (numbers.Length > listIndex + 1)
                {
                    listIndex++;
                }
                else
                {
                    break;
                }
            }
            return this;
        }


        public override string ToString()
        {
            var result = "vector: {";
            for (int i = 0; i < vectorSize; i++)
            {
                result += vector[i];
                if (i < vectorSize - 1)
                {
                    result += ", ";
                }
            }
            result += "}";
            return result;
        }


        //operators
        public static Vector operator +(Vector v1, Vector v2)
        {
            return v1.AddVector(v2);
        }

        public static Vector operator -(Vector v1, Vector v2)
        {
            return v1.MinusVector(v2);
        }

        public static Vector operator *(Vector v1, int number)
        {
            return v1.MultVectorByInt(number);
        }

        public static Vector operator *(int number, Vector v1)
        {
            return v1.MultVectorByInt(number);
        }

        public static int operator *(Vector v1, Vector v2)
        {
            return v1.MultVector(v2);
        }


        public static double Length(Vector v1)
        {
            return v1.Determinant();
        }
    }
}
