﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Antlr.Runtime;
using Antlr.Runtime.Tree;
using MatrixCompiler.CodeGeneration;
using MatrixCompiler.Files;
using MatrixCompiler.SyntaxAnalize;
using MatrixCompiler.Test;

namespace MatrixCompiler
{
    public partial class Form1 : Form
    {
        private StringBuilder _strBuilderView;
        private const string TestCode = @"Matrix c = Matrix[1,3](12,55);

Void Main(){
Println(c);
Println(c[0,0]);
Int result = c[0,0] * 2;
Matrix mat = c * 3;
Println(result);
Println(mat);
Int det = Matrix.Det(c);
Println(det);
}";

        public Form1()
        {
            InitializeComponent();
            txtCode.Text = TestCode;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void btnCompile_Click(object sender, EventArgs e)
        {
            //Compile();  
            PrintCodeTree();
        }


        private void Compile()
        {
            try
            {
                var programRunner = new ProgramRunner();
                programRunner.RemoveExe();

                var code = txtCode.Text;
                var syntaxAnalizer = new SyntaxAnalizer();
                var codeGenerator = new CodeGenerator(syntaxAnalizer);
                var codeTree = BuildCodeTree(code);
                var cSharpCode = codeGenerator.Generate(code, codeTree);
                MessageBox.Show(cSharpCode);
                CSFileWriter.WriteCSFile(cSharpCode);
                BatFileWriter.WriteBatFile();

                programRunner.ExecuteFiles();             
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }


        private ITree BuildCodeTree(string code)
        {
            var input = new ANTLRStringStream(code);
            var lexer = new MatrixCompilerGrammarLexer(input);
            var tokens = new CommonTokenStream(lexer);
            var parser = new MatrixCompilerGrammarParser(tokens);

            var tree = parser.code().Tree;
            return tree;
        }



        /// <summary>
        /// Helpers
        /// </summary>

        private void PrintCodeTree()
        {
           PrintTree(BuildCodeTree(txtCode.Text));          
        }


        private void PrintTree(ITree tree)
        {
            _strBuilderView = new StringBuilder();
            ViewTree(tree, 0);
            MessageBox.Show(_strBuilderView.ToString(), "Tree view");
        }

        private void ViewTree(ITree tree, int tab)
        {
            if (tree == null)
            {
                return;
            }

            for (int i = 0; i < tab; i++)
            {
                _strBuilderView.Append(" | ");
            }

            _strBuilderView.Append(string.Format("{0}  <-- Type: {1}\n"
                    , tree, tree.Type));

            for (int i = 0; i < tree.ChildCount; i++)
            {
                ViewTree(tree.GetChild(i), tab + 1);
            }
        }

    }
}
