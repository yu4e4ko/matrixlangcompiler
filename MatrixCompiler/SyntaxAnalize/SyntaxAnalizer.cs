﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime.Tree;

namespace MatrixCompiler.SyntaxAnalize
{
    internal class SyntaxAnalizer
    {
        public Dictionary<string, int> VarDefine { get; private set; }
        public Dictionary<string, int> FunctionDefine { get; private set; }
        public List<string> Variables { get; private set; }
        public List<string> Functions { get; private set; }

        public string CurrentFuncType { get; set; }
        public string CurrentFuntionName { get; set; }
        public bool IsCurrentFuncHaveReturn { get; set; }

        

        public SyntaxAnalizer()
        {
            VarDefine = new Dictionary<string, int>();
            FunctionDefine = new Dictionary<string, int>();
            Variables = new List<string>();
            Functions = new List<string>();
            CurrentFuntionName = string.Empty;
        }


        public void AddVarDefine(string varName)
        {
            if (!VarDefine.ContainsKey(varName))
            {
                VarDefine.Add(varName, 1);
            }
            else
            {
                varName = varName.Substring(varName.IndexOf('_') + 1);
                throw new Exception(varName + " already defined");
            }
        }


        public void AddFunctionDefine(string funcName)
        {
            if (!FunctionDefine.ContainsKey(funcName))
            {
                FunctionDefine.Add(funcName, 1);
            }
            else
            {
                throw new Exception("funtion " + funcName + " already defined");
            }
        }


        public void AddFunction(string funcName)
        {
            if (!Functions.Contains(funcName))
            {
                Functions.Add(funcName);
            }
        }


        public void AddVariable(string varName)
        {
            if (!Variables.Contains(varName))
            {
                Variables.Add(varName);
            }
        }


        public void CheckParens(string code)
        {
            var rParenCount = code.Count(x => x.Equals(')'));
            var lParenCount = code.Count(x => x.Equals('('));
            if (rParenCount != lParenCount)
            {
                throw new Exception("number of left parentheses and " +
                                    "the number of right parentheses does not match");
            }
        }

        public void CheckBraces(string code)
        {
            var rBrace = code.Count(x => x.Equals('}'));
            var lBrace = code.Count(x => x.Equals('{'));
            if (rBrace != lBrace)
            {
                throw new Exception("number of left braces and " +
                                    "the number of right braces does not match");
            }
        }


        public void CheckVariables()
        {
            foreach (var variable in Variables)
            {
                if (!VarDefine.ContainsKey(variable))
                {
                    var varName = variable.Substring(variable.IndexOf('_') + 1);
                    throw new Exception(varName + " not defined");
                }
            }
        }


        public void CheckFuntions()
        {
            foreach (var function in Functions)
            {
                if (!FunctionDefine.ContainsKey(function))
                {
                    throw new Exception("funtion " + function + " not defined");
                }
            }
        }



        public void CheckRetunr()
        {
            if (CurrentFuncType != "void" && !string.IsNullOrEmpty(CurrentFuncType))
            {
                if (!IsCurrentFuncHaveReturn)
                {
                    throw new Exception(string.Format("function {0} must return value"
                        , CurrentFuntionName));
                }
            }
            else if (CurrentFuncType == "void" && !string.IsNullOrEmpty(CurrentFuncType))
            {
                if (IsCurrentFuncHaveReturn)
                {
                    throw new Exception(string.Format("function {0} must not return value"
                        , CurrentFuntionName));
                }
            }
            IsCurrentFuncHaveReturn = false;
        }


        public void CheckSyntax(ITree tree)
        {
            ViewTree(tree);
        }

        private void ViewTree(ITree tree)
        {
            if (tree == null)
            {
                return;
            }
            if (tree.ToString().Contains("<mis"))
            {
                throw new Exception(string.Format("Syntax error in {0} line", tree.Line));
            }

            for (int i = 0; i < tree.ChildCount; i++)
            {
                ViewTree(tree.GetChild(i));
            }
        }
    }
}
