﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime.Tree;
using MatrixCompiler.SyntaxAnalize;

namespace MatrixCompiler.CodeGeneration
{
    internal class CodeGenerator
    {
        private readonly SyntaxAnalizer _syntaxAnalizer;
        private ITree _codeTree;

        public CodeGenerator(SyntaxAnalizer syntaxAnalizer)
        {
            _syntaxAnalizer = syntaxAnalizer;
        }


        public string Generate(string code, ITree codeTree)
        {
            _codeTree = codeTree;
            _syntaxAnalizer.CheckSyntax(codeTree);
            CheckParensAndBraces(code);

            var cSharpCodeGen = new CSharpCodeGenerator(_syntaxAnalizer, _codeTree);
            var cSharpCode = cSharpCodeGen.Generate();

            _syntaxAnalizer.CheckVariables();
            _syntaxAnalizer.CheckFuntions();
            return cSharpCode;
        }


        private void CheckParensAndBraces(string code)
        {
            _syntaxAnalizer.CheckBraces(code);
            _syntaxAnalizer.CheckParens(code);
        }
    }
}
