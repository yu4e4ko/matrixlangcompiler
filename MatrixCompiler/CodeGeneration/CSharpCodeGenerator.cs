﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antlr.Runtime.Tree;
using MatrixCompiler.SyntaxAnalize;
using MatrixCompiler.Test;

namespace MatrixCompiler.CodeGeneration
{
    internal class CSharpCodeGenerator
    {
        private readonly SyntaxAnalizer _syntaxAnalizer;
        private readonly ITree _codeTree;
        private StringBuilder _codeStringBuilder;

        private const string WRAP_TEXT = "using System; using System.Text;" +
                                         " using MatrixLib.Vector; using MatrixLib.Matrix;\n" +
                                         " public class Program{";

        public CSharpCodeGenerator(SyntaxAnalizer syntaxAnalizer, ITree tree)
        {
            _syntaxAnalizer = syntaxAnalizer;
            _codeTree = tree;
        }


        public string Generate()
        {
            _codeStringBuilder = new StringBuilder();
            _codeStringBuilder.AppendLine(WRAP_TEXT);
            WalkTree(_codeTree);
            _codeStringBuilder.AppendLine("}");
            return _codeStringBuilder.ToString();
        }


        /// <summary>
        /// Append to stringBuilder
        /// </summary>
        /// <param name="tree"></param>
        private void AppendWhitSpaces(string text)
        {
            _codeStringBuilder.Append(string.Format(" {0} ", text));
        }

        private void AppendSemicolon()
        {
            _codeStringBuilder.Append(";" + Environment.NewLine);
        }

        /// <summary>
        /// Add function prefix
        /// </summary>
        /// <param name="varName"></param>
        private string AddFunctionPrefix(string varName)
        {
            var result = string.Empty;
            return _syntaxAnalizer.CurrentFuntionName + "_" + varName;
        }

        private string GetVarWithFuncPrefix(string varName)
        {
            var varNameWhithPrefix = _syntaxAnalizer.CurrentFuntionName + "_" + varName;
            if (_syntaxAnalizer.VarDefine.ContainsKey(varNameWhithPrefix))
            {
                return varNameWhithPrefix;
            }
            varName = "_" + varName;
            _syntaxAnalizer.AddVariable(varName);
            return varName;
        }


        /// <summary>
        /// Generating code
        /// </summary>
        /// <param name="tree"></param>
        private void WalkTree(ITree tree)
        {
            var childCount = tree.ChildCount;
            for (var i = 0; i < childCount; i++)
            {
                var child = tree.GetChild(i);
                switch (child.Type)
                {
                    case MatrixCompilerGrammarParser.VAR_DEFINE:
                        VarDefineHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.FUNCTION:
                        FunctionHandler(child);
                        break;

                }
            }
        }

        #region functionDefining

        private void FunctionHandler(ITree tree)
        {
            for (int i = 0; i < tree.ChildCount; i++)
            {
                var child = tree.GetChild(i);
                switch (child.Type)
                {
                    case MatrixCompilerGrammarParser.MAIN_FUNC:
                        MainFuntionHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.FUNC_TYPE:
                        FuntionTypeHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.FUNC_NAME:
                        FunctionNameHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.FUNC_PARAMS:
                        FunctionParamsHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.FUNC_BODY:
                        var rParenAfterParams = ")";
                        var beginFuncBlock = "{";
                        AppendWhitSpaces(rParenAfterParams);
                        AppendWhitSpaces(beginFuncBlock + Environment.NewLine);
                        FunctionBodyHandler(child);
                        var endFuncBlock = "}";
                        AppendWhitSpaces(endFuncBlock + Environment.NewLine);
                        break;
                }     
            }
            _syntaxAnalizer.CheckRetunr();
        }


        private void FunctionParamsHandler(ITree tree)
        {
            for (int i = 0; i < tree.ChildCount; i++)
            {
                var param = tree.GetChild(i);
                for (int j = 0; j < param.ChildCount; j++)
                {
                    var paramChild = param.GetChild(j);
                    switch (paramChild.Type)
                    {
                        case MatrixCompilerGrammarParser.VAR_TYPE:
                            VarTypeHandler(paramChild);
                            break;
                        case MatrixCompilerGrammarParser.VAR_NAME:
                            VarDefineNameHandler(paramChild);
                            break;
                    }
                }
                if (i < tree.ChildCount - 1)
                {
                    AppendWhitSpaces(", ");
                }
            }
        }


        private void FunctionNameHandler(ITree tree)
        {
            var funcName = tree.GetChild(0).ToString();
            AppendWhitSpaces(funcName);
            _syntaxAnalizer.CurrentFuntionName = funcName;
            _syntaxAnalizer.AddFunctionDefine(funcName);
            var lparenForParams = "(";
            AppendWhitSpaces(lparenForParams);
        }


        private void FuntionTypeHandler(ITree tree)
        {
            var funcType = Dictionaries.FunctionTypes[tree.GetChild(0).ToString()];
            _syntaxAnalizer.CurrentFuncType = funcType;
            AppendWhitSpaces("static " + funcType);
        }

        private void MainFuntionHandler(ITree tree)
        {
            AppendWhitSpaces("static void Main(string[] args){\n");
            _syntaxAnalizer.CurrentFuncType = "void";
            _syntaxAnalizer.AddFunctionDefine("Main");
            _syntaxAnalizer.CurrentFuntionName = "Main";
            AppendWhitSpaces("try{\n");
            FunctionBodyHandler(tree.GetChild(0));
            AppendWhitSpaces("\n}catch(Exception ex){Console.WriteLine(ex.Message);}\n" +
                             "finally{Console.ReadLine();}\n}\n");
        }


        private void FunctionBodyHandler(ITree tree)
        {
            for (int i = 0; i < tree.ChildCount; i++)
            {
                var child = tree.GetChild(i);
                switch (child.Type)
                {
                    case MatrixCompilerGrammarParser.VAR_DEFINE:
                        VarDefineHandler(child);
                        AppendSemicolon();
                        break;
                    case MatrixCompilerGrammarParser.VAR_ASSIGN:
                        VarAssignHandler(child);
                        AppendSemicolon();
                        break;
                    case MatrixCompilerGrammarParser.IF_OPERATOR:
                        IfOperatorHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.WHILE_OPERATOR:
                        WhileOperatorHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.FOR:
                        ForOperatorHandeler(child);
                        break;
                    case MatrixCompilerGrammarParser.RETURN:
                        ReturnFuncHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.FUNCTION_CALL:
                        FuntionCallHandler(child);
                        AppendSemicolon();
                        break;
                }
            }
        }


        private void ReturnFuncHandler(ITree tree)
        {
            AppendWhitSpaces("return");
            MathExprHandler(tree.GetChild(0));
            _syntaxAnalizer.IsCurrentFuncHaveReturn = true;
            AppendSemicolon();
        }

        private void ForOperatorHandeler(ITree tree)
        {
            AppendWhitSpaces("for(");
            var firstBlock = tree.GetChild(2);
            var secondBlock = tree.GetChild(4);
            var thirdBlock = tree.GetChild(6);

            ITree funcBody = null;
            for (int i = 0; i < tree.ChildCount; i++)
            {
                if (tree.GetChild(i).Type == MatrixCompilerGrammarParser.FUNC_BODY)
                {
                    funcBody = tree.GetChild(i);
                }
            }
            
            VarAssignHandler(firstBlock);
            AppendWhitSpaces(";");
            ComparisonHandler(secondBlock);
            AppendWhitSpaces(";"); 
            
            VarAssignHandler(thirdBlock);
            AppendWhitSpaces("){\n");
            FunctionBodyHandler(funcBody);
            AppendWhitSpaces("}\n");        
        }

        private void WhileOperatorHandler(ITree tree)
        {
            AppendWhitSpaces("while(");
            for (int i = 0; i < tree.ChildCount; i++)
            {
                var child = tree.GetChild(i);
                switch (child.Type)
                {
                    case MatrixCompilerGrammarParser.COMPARISON:
                        ComparisonHandler(child);
                        AppendWhitSpaces(")\n{\n");
                        break;
                    case MatrixCompilerGrammarParser.FUNC_BODY:
                        FunctionBodyHandler(child);
                        AppendWhitSpaces("}\n");
                        break;
                }
            }
        }

        private void IfOperatorHandler(ITree tree)
        {
            AppendWhitSpaces("if(");
            for (int i = 0; i < tree.ChildCount; i++)
            {
                var child = tree.GetChild(i);
                switch (child.Type)
                {
                    case MatrixCompilerGrammarParser.COMPARISON:
                        ComparisonHandler(child);
                        AppendWhitSpaces(")\n{\n");
                        break;
                    case MatrixCompilerGrammarParser.FUNC_BODY:
                        FunctionBodyHandler(child);
                        AppendWhitSpaces("}\n");
                        break;
                    case MatrixCompilerGrammarParser.ELSE_OPERATOR:
                        ElseHandler(child);
                        break;
                }
            }
        }


        private void ElseHandler(ITree tree)
        {
            AppendWhitSpaces("else\n{\n");
            for (int i = 0; i < tree.ChildCount; i++)
            {
                var child = tree.GetChild(i);
                switch (child.Type)
                {
                    case MatrixCompilerGrammarParser.FUNC_BODY:
                        FunctionBodyHandler(child);
                        break;
                }
            }
            AppendWhitSpaces("}\n");
        }


        private void ComparisonHandler(ITree tree)
        {
            MathExprHandler(tree.GetChild(0));
            var comparisonOperation = tree.GetChild(1).GetChild(0).ToString();
            AppendWhitSpaces(comparisonOperation);
            MathExprHandler(tree.GetChild(2));
        }


        private void FuntionCallHandler(ITree tree)
        {
            for (int i = 0; i < tree.ChildCount; i++)
            {
                var child = tree.GetChild(i);
                switch (child.Type)
                {
                    case MatrixCompilerGrammarParser.FUNC_NAME:
                        FunctionNameCallHandler(child);
                        AppendWhitSpaces("(");
                        break;
                    case MatrixCompilerGrammarParser.ARGS:
                        ArgsHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.PRINT_FUNC:
                        PrintFunctionHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.VECTOR_LEN_FUNC:
                        VectorLengthFuncHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.MATRIX_DET_FUNC:
                        MatrixDedFuncHandler(child);
                        break;
                }
            }
            AppendWhitSpaces(")");
        }


        private void VectorLengthFuncHandler(ITree tree)
        {
            var functionCode = "Vector.Len(";
            AppendWhitSpaces(functionCode);
            ArgsHandler(tree.GetChild(0));
        }


        private void MatrixDedFuncHandler(ITree tree)
        {
            var functionCode = "Matrix.Determinant(";
            AppendWhitSpaces(functionCode);
            ArgsHandler(tree.GetChild(0));
        }


        private void PrintFunctionHandler(ITree tree)
        {
            var functionCode = "Console.WriteLine(";
            AppendWhitSpaces(functionCode);
            ArgsHandler(tree.GetChild(0));
            AppendWhitSpaces("+Environment.NewLine + \"--------------------------------\" +Environment.NewLine");
        }

        private void FunctionNameCallHandler(ITree tree)
        {
            var funcName = tree.GetChild(0).ToString();
            _syntaxAnalizer.Functions.Add(funcName);
            AppendWhitSpaces(funcName);
        }

        #endregion functionDefining


        #region variables
        private void VarAssignHandler(ITree tree)
        {
            for (int i = 0; i < tree.ChildCount; i++)
            {
                var child = tree.GetChild(i);
                switch (child.Type)
                {
                    case MatrixCompilerGrammarParser.VAR_NAME:
                        VarNameHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.ASSING_OP:
                        AssignOperatorHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.CAST:
                        VarCastHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.MATH_EXPR:
                        MathExprHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.MATRIX_CREATING:
                        MatrixCreatingHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.FUNCTION_CALL:
                        FuntionCallHandler(child);
                        break;
                }
            }
            //AppendSemicolon();
        }




        private void VarDefineHandler(ITree tree)
        {
            //if global var        
            if (string.IsNullOrEmpty(_syntaxAnalizer.CurrentFuntionName))
            {
                AppendWhitSpaces("static");
            }
            for (var i = 0; i < tree.ChildCount; i++)
            {
                var child = tree.GetChild(i);
                switch (child.Type)
                {
                    case MatrixCompilerGrammarParser.VAR_TYPE:
                        VarTypeHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.VAR_ASSIGN:
                        VarDefineAssignHandler(child);
                        break;
                    
                }
            }
            AppendSemicolon();
        }


        private void VarCastHandler(ITree tree)
        {
            AppendWhitSpaces("(");
            VarTypeHandler(tree.GetChild(0));
            AppendWhitSpaces(")");
        }

        private void AssignOperatorHandler(ITree tree)
        {
            var assignOp = tree.GetChild(0).ToString();
            AppendWhitSpaces(assignOp);
        }

        private void VarDefineAssignHandler(ITree tree)
        {
            for (int i = 0; i < tree.ChildCount; i++)
            {
                var child = tree.GetChild(i);
                switch (child.Type)
                {
                    case MatrixCompilerGrammarParser.VAR_NAME:
                        VarDefineNameHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.ASSING_OP:
                        AssignOperatorHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.CAST:
                        VarCastHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.MATH_EXPR:
                        MathExprHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.MATRIX_CREATING:
                        MatrixCreatingHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.FUNCTION_CALL:
                        FuntionCallHandler(child);
                        break;
                }   
            }
        }


        private void MatrixCreatingHandler(ITree tree)
        {
            AppendWhitSpaces("new");
            for (int i = 0; i < tree.ChildCount; i++)
            {
                var child = tree.GetChild(i);
                switch (child.Type)
                {
                    case MatrixCompilerGrammarParser.VAR_TYPE:
                        VarTypeHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.INDEXES:
                        MatrixCreatingIndexesHandler(child);
                        break;
                    case MatrixCompilerGrammarParser.ARGS:
                        MatrixFillHandler(child);
                        break;
                }
            }
            //AppendSemicolon();
        }


        private void MatrixFillHandler(ITree tree)
        {
            AppendWhitSpaces(".Fill(");
            ArgsHandler(tree);
            AppendWhitSpaces(")");
        }

        private void ArgsHandler(ITree tree)
        {
            for (int i = 0; i < tree.ChildCount; i++)
            {
                var mathExpr = tree.GetChild(i);
                MathExprHandler(mathExpr);
                if (i < tree.ChildCount - 1)
                {
                    AppendWhitSpaces(",");
                }
            }
        }


        private void MatrixCreatingIndexesHandler(ITree tree)
        {
            AppendWhitSpaces("(");
            for (int i = 0; i < tree.ChildCount; i++)
            {
                AppendWhitSpaces(tree.GetChild(i).ToString());
                if (i < tree.ChildCount - 1)
                {
                    AppendWhitSpaces(",");
                }
            }
            AppendWhitSpaces(")");
        }


        public void MathExprHandler(ITree tree)
        {
            int childrenCount = tree.ChildCount;
            
            if (tree.Type == MatrixCompilerGrammarParser.BRACKETS)
            {
                AppendWhitSpaces("(");
                MathExprHandler(tree.GetChild(0));
                AppendWhitSpaces(")");
            }
            else if (tree.Type == MatrixCompilerGrammarParser.VAR_NAME)
            {
                VarNameHandler(tree);
            }
            else if (tree.Type == MatrixCompilerGrammarParser.POW)
            {
                AppendWhitSpaces("Math.Pow(");
                MathExprHandler(tree.GetChild(0));
                AppendWhitSpaces(", ");
                MathExprHandler(tree.GetChild(1));
                AppendWhitSpaces(")");
            }
            else if (childrenCount == 0)
            {
                AppendWhitSpaces(tree.ToString());
            }
            else if (childrenCount == 1)
            {
                MathExprHandler(tree.GetChild(0));
            }
            else if (childrenCount == 2)
            {
                MathExprHandler(tree.GetChild(0));
                AppendWhitSpaces(tree.ToString());
                MathExprHandler(tree.GetChild(1));
            } 
        }

        private void VarNameHandler(ITree tree)
        {
            var varName = tree.GetChild(0).ToString();
            varName = GetVarWithFuncPrefix(varName);
            AppendWhitSpaces(varName);
            if (tree.ChildCount > 1)
            {
                var indexatorChild = tree.GetChild(1);
                if (indexatorChild.Type == MatrixCompilerGrammarParser.INDEXATOR)
                {
                    IndexatorHandler(indexatorChild);
                }
            }
        }


        private void IndexatorHandler(ITree tree)
        {
            var indexes = tree.GetChild(0);
            AppendWhitSpaces("[");
            for (int i = 0; i < indexes.ChildCount; i++)
            {
                AppendWhitSpaces(indexes.GetChild(i).ToString());
                if (i < indexes.ChildCount - 1)
                {
                    AppendWhitSpaces(",");
                }
            }
            AppendWhitSpaces("]");
        }



        private void VarDefineNameHandler(ITree tree)
        {
            var varName = tree.GetChild(0).ToString();
            varName = AddFunctionPrefix(varName);
            _syntaxAnalizer.AddVarDefine(varName);
            AppendWhitSpaces(varName);
        }

        private void VarTypeHandler(ITree tree)
        {
            var varType = tree.GetChild(0).ToString();
            if (Dictionaries.VariableTypes.ContainsKey(varType))
            {
                varType = Dictionaries.VariableTypes[varType];
                AppendWhitSpaces(varType);
            }
            else
            {
                throw new Exception(varType  + " - incorrect type");
            }
        }

#endregion varible

    }
}
