﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixCompiler.CodeGeneration
{
    internal class Dictionaries
    {
        public static Dictionary<string, string> VariableTypes 
            = new Dictionary<string, string>()
            {
                {"Int", "int"},
                {"Matrix", "Matrix"},
                {"Vector", "Vector"}
            };


        public static Dictionary<string, string> FunctionTypes
            = new Dictionary<string, string>()
            {
                {"Int", "int"},
                {"Matrix", "Matrix"},
                {"Vector", "Vector"},
                {"Void", "void"}
            };

    }
}
