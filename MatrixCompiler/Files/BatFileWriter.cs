﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixCompiler.Files
{
    class BatFileWriter
    {
        public const string BAT_FILE_NAME = "Compile.bat";
        private const string FILE_CONTENT = "csc.exe /out:Program.exe /t:exe "
                    + "/r:MSCorLib.dll /r:MatrixLib.dll Program.cs\n pause";

        public static void WriteBatFile()
        {
            using (var stream = new StreamWriter(BAT_FILE_NAME))
            {
                stream.WriteLine(FILE_CONTENT);
            }

        }
    }
}
