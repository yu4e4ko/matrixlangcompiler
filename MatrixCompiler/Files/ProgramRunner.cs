﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MatrixCompiler.Files
{
    class ProgramRunner
    {
        private const string EXE_FILE_NAME = "Program.exe";

        public void ExecuteFiles()
        {
            RunBat();
            Thread.Sleep(2000);
            RunExe();
        }



        private void RunBat()
        {
            var pInfo = new ProcessStartInfo();
            pInfo.FileName = BatFileWriter.BAT_FILE_NAME;
            pInfo.UseShellExecute = true;
            var p = Process.Start(pInfo);
        }


        private void RunExe()
        {
            if (!File.Exists(EXE_FILE_NAME))
            {
                throw new Exception("Compiling failed...");
            }
            var pInfo = new ProcessStartInfo();
            pInfo.FileName = EXE_FILE_NAME;
            pInfo.UseShellExecute = true;
            var p = Process.Start(pInfo);
        }


        public void RemoveExe()
        {
            if (File.Exists(EXE_FILE_NAME))
            {
                File.Delete(EXE_FILE_NAME);
            }
        }

    }
}
