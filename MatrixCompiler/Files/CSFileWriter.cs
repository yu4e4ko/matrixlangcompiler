﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixCompiler.Files
{
    class CSFileWriter
    {
        public const string CS_FILE_NAME = "Program.cs";
        public static void WriteCSFile(string code)
        {
            using (var file = new StreamWriter(CS_FILE_NAME))
            {
                file.WriteLine(code);
            }

        }
    }
}
